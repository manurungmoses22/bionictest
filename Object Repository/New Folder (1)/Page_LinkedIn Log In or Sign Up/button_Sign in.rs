<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Sign in</name>
   <tag></tag>
   <elementGuidId>4ad33ba5-2ca2-492d-a184-de084209439a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn-md.btn-primary.flex-shrink-0.cursor-pointer.sign-in-form__submit-btn--full-width</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='submit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>1b688d4e-3c69-430f-b6a5-6c6f2e2e4182</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn-md btn-primary flex-shrink-0 cursor-pointer
            sign-in-form__submit-btn--full-width</value>
      <webElementGuid>a1b22621-f152-4b30-8b05-de75dcf13762</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-id</name>
      <type>Main</type>
      <value>sign-in-form__submit-btn</value>
      <webElementGuid>3ca7537e-1179-4dba-b2f7-11be87a167a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-tracking-control-name</name>
      <type>Main</type>
      <value>homepage-basic_sign-in-submit-btn</value>
      <webElementGuid>063b7020-1406-48d6-adfb-83d17f21018a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>4252dde5-1d59-4880-aedc-618d614f28b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
          Sign in
        </value>
      <webElementGuid>ef4f7000-03f1-4633-bbaf-6d79eec985ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content&quot;)/section[@class=&quot;section min-h-[560px] flex-nowrap pt-[40px] babybear:flex-col babybear:min-h-[0] babybear:px-mobile-container-padding babybear:pt-[24px]&quot;]/div[@class=&quot;self-start relative flex-shrink-0 w-[55%] pr-[42px] babybear:w-full babybear:pr-[0px]&quot;]/div[@class=&quot;hero-cta-form&quot;]/form[1]/div[@class=&quot;flex justify-between
          sign-in-form__footer--full-width&quot;]/button[@class=&quot;btn-md btn-primary flex-shrink-0 cursor-pointer
            sign-in-form__submit-btn--full-width&quot;]</value>
      <webElementGuid>25ca1730-e23f-4ce3-970d-49eb63b14c42</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='submit']</value>
      <webElementGuid>816fdebb-1bcc-4f30-b541-82c7e6d2a8a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//main[@id='main-content']/section/div/div/form/div[2]/button</value>
      <webElementGuid>6bff8206-d2e5-48ba-b034-25464ba34c09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot password?'])[1]/following::button[1]</value>
      <webElementGuid>73ea88af-d56f-4867-bc4a-6081f5928a19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show'])[1]/following::button[1]</value>
      <webElementGuid>22badc46-f996-490e-8434-5a5e5e78138f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign in with Google'])[1]/preceding::button[1]</value>
      <webElementGuid>416fbc8b-0660-46f2-b19c-7ee2c116ce51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New to LinkedIn? Join now'])[1]/preceding::button[2]</value>
      <webElementGuid>623a29af-944f-4ab9-bd2b-ae0f2d5cf278</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button</value>
      <webElementGuid>c56e9efb-225e-4553-a5f1-702e4570e083</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and (text() = '
          Sign in
        ' or . = '
          Sign in
        ')]</value>
      <webElementGuid>41cf7404-f60d-4962-ab77-a0619a33d10d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
