<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Selesai</name>
   <tag></tag>
   <elementGuidId>d1471c67-6430-4eb4-9f0a-500f819cba28</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ember198 > span.artdeco-button__text</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '
    
          Selesai
      
' or . = '
    
          Selesai
      
')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='ember198']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>2a4859b9-2792-431a-b675-03c341766b33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>artdeco-button__text</value>
      <webElementGuid>5be19e88-4507-4534-b3c4-3c4aeeab5575</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
          Selesai
      
</value>
      <webElementGuid>66574351-ef93-4ec7-a893-a7018a142c6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ember198&quot;)/span[@class=&quot;artdeco-button__text&quot;]</value>
      <webElementGuid>29db8a54-338f-468d-8ff3-8cf834c5b028</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//button[@id='ember198']/span</value>
      <webElementGuid>129e9fa6-deb8-48ed-ab54-3aa93e201d7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Akhir konten dialog.'])[1]/preceding::span[1]</value>
      <webElementGuid>2d562654-c426-4091-972f-26db598cdf9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Selesai']/parent::*</value>
      <webElementGuid>d60b0183-5ee4-4e1a-a07d-647b39e0b154</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/button[2]/span</value>
      <webElementGuid>77c7f4b9-865f-4d8d-8821-515eb086aea7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
    
          Selesai
      
' or . = '
    
          Selesai
      
')]</value>
      <webElementGuid>fd87ef2a-b032-4e25-92f8-ba4218e6e5fa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
