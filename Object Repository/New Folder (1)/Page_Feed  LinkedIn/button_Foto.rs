<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Foto</name>
   <tag></tag>
   <elementGuidId>a8a2f8c1-6430-4272-af22-a13154f8389a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ember25</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='ember25']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>b1afba6f-c0a4-41b0-abcf-0ec6650c1397</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Tambahkan foto</value>
      <webElementGuid>c051d6ca-c68e-4fb5-8082-26915e3f99ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ember25</value>
      <webElementGuid>5b35a105-a581-424c-a589-63ad63769538</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>artdeco-button artdeco-button--muted artdeco-button--4 artdeco-button--tertiary ember-view share-box-feed-entry-toolbar__item</value>
      <webElementGuid>ac117aec-1f09-4b17-8ca1-262b1292989d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-artdeco-is-focused</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>c9b70e79-47b8-40b8-8a75-95455d4fd6e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>  
  



    Foto
</value>
      <webElementGuid>799a4f65-d945-4bb9-bc9f-ff54964920f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ember25&quot;)</value>
      <webElementGuid>b54baf50-d114-4204-bf92-7f7e005be6f4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='ember25']</value>
      <webElementGuid>b2b77627-6dfe-4614-afc5-5c8916ecebda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ember21']/div[2]/div/div/main/div/div[2]/div[3]/button</value>
      <webElementGuid>2446b087-7e0e-450d-8a35-bdaead8669dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/button</value>
      <webElementGuid>29e9a01e-793f-4aa8-a0f3-cb2bc877c958</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'ember25' and (text() = '  
  



    Foto
' or . = '  
  



    Foto
')]</value>
      <webElementGuid>e432c895-5e8d-4ff2-8d71-74030274341e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
