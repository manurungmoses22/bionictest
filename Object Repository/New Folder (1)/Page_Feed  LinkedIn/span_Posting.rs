<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Posting</name>
   <tag></tag>
   <elementGuidId>2bfaab9b-e346-4a89-856f-f3b9214adef8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ember221 > span.artdeco-button__text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='ember221']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>c1092bb3-ad19-46d2-89bd-e4aafb07568a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>artdeco-button__text</value>
      <webElementGuid>bfa3b324-8edf-4eed-8819-3ffb4be2cf5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
          Posting
      
</value>
      <webElementGuid>9fb41f9c-6828-4d91-9f1b-fcc4df4271c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ember221&quot;)/span[@class=&quot;artdeco-button__text&quot;]</value>
      <webElementGuid>12c69f64-8984-4aae-aecf-d1209dadebce</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//button[@id='ember221']/span</value>
      <webElementGuid>eb8f14d3-10d1-47b8-8a90-0364d6f059f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hapus media'])[1]/following::span[6]</value>
      <webElementGuid>66a21284-44ec-4650-895e-5a232ae35f70</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Akhir konten dialog.'])[1]/preceding::span[1]</value>
      <webElementGuid>50a8b601-784a-47ae-9df1-589e59111ff5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Posting']/parent::*</value>
      <webElementGuid>6997c5ed-dc8b-498a-ad98-03284b3f55a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button/span</value>
      <webElementGuid>b38ee3c6-129c-4d3e-98f3-cf25700b6658</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
    
          Posting
      
' or . = '
    
          Posting
      
')]</value>
      <webElementGuid>2fcc1fa9-eb62-4974-8931-f72472f4ed24</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
