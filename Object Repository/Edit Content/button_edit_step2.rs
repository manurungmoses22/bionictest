<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_edit_step2</name>
   <tag></tag>
   <elementGuidId>71f44f5a-2b4e-47a5-a198-bfb2bccdeb38</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h5[text()='Edit posting']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h5[text()='Edit posting']</value>
      <webElementGuid>deb7f021-56d3-470e-b1a4-8237357ebf8c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
