<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_field</name>
   <tag></tag>
   <elementGuidId>2a6a456f-59e8-41a3-8f60-69baebead639</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class='ql-editor ql-blank']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class='ql-editor ql-blank']</value>
      <webElementGuid>1751965d-dee7-4c11-b2a5-6ed2e0e0b81c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
