<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_field</name>
   <tag></tag>
   <elementGuidId>e2532fbd-f10a-4b73-854c-6596b6bcaa3f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class='ql-editor ql-blank']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class='ql-editor ql-blank']</value>
      <webElementGuid>b5d6cb07-fc92-47fb-85d2-c4361aa4a731</webElementGuid>
   </webElementProperties>
</WebElementEntity>
