<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>password_field</name>
   <tag></tag>
   <elementGuidId>6ee66fa7-e6cb-4821-be53-fbd0bd650f8d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id=&quot;session_password&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id=&quot;session_password&quot;]</value>
      <webElementGuid>a1f2a54e-bea5-4083-9523-72c06c7af668</webElementGuid>
   </webElementProperties>
</WebElementEntity>
