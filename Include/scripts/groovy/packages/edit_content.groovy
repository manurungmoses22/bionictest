package packages
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class edit_content {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */

	@When("User click action button")
	def user_click_action_btn() {
		WebUI.callTestCase(findTestCase('component/edit_content/bttn_edit_step1'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click edit button")
	def user_click_edit_btn() {
		WebUI.callTestCase(findTestCase('component/edit_content/bttn_edit_step_2'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input content edit")
	def input_content_edit() {
		WebUI.callTestCase(findTestCase('component/edit_content/text_field'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click submit button")
	def click_submit_btn() {
		WebUI.callTestCase(findTestCase('component/edit_content/bttn_simpan'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}